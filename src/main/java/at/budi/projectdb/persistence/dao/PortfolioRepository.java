package at.budi.projectdb.persistence.dao;

import at.budi.projectdb.persistence.entity.Portfolio;
import org.springframework.data.repository.CrudRepository;

public interface PortfolioRepository extends CrudRepository<Portfolio, Long> {

}
