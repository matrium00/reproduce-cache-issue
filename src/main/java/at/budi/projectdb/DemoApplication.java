package at.budi.projectdb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
//@EnableCaching //does not work, only enables Springs method-level cache, does nothing for JPA
public class DemoApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoApplication.class);

    @Value("${maven.project.version}")
    private String applicationVersion;

    @PostConstruct
    public void postConstruct() {
        LOGGER.info("resource filtering extracted maven version from POM: {}", applicationVersion);
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
