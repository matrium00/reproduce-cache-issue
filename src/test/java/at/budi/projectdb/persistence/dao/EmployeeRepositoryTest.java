package at.budi.projectdb.persistence.dao;

import at.budi.projectdb.persistence.entity.Portfolio;
import net.sf.ehcache.CacheManager;
import org.hibernate.jpa.internal.EntityManagerFactoryImpl;
import org.hibernate.stat.Statistics;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.orm.jpa.EntityManagerFactoryInfo;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Iterator;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class EmployeeRepositoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeRepositoryTest.class);

    @Autowired
    private PortfolioRepository portfolioRepo;

    @PersistenceContext
    private EntityManager em;

    @Test
    @Transactional
    public void testL2Cache() {
        EntityManagerFactoryInfo entityManagerFactoryInfo = (EntityManagerFactoryInfo) em.getEntityManagerFactory();
        EntityManagerFactoryImpl emfImp = (EntityManagerFactoryImpl) entityManagerFactoryInfo.getNativeEntityManagerFactory();
        emfImp.getProperties();
        Statistics statistics = emfImp.getSessionFactory().getStatistics();

        portfolioRepo.save(new Portfolio("Test-Portfolio 1"));
        portfolioRepo.save(new Portfolio("Test-Portfolio 2"));
        portfolioRepo.save(new Portfolio("Test-Portfolio 3"));

        Iterator<Portfolio> portfolios = portfolioRepo.findAll().iterator();
        assertTrue(portfolios.hasNext());
        Long portfolioId = portfolios.next().getId();

        emfImp.getCache().evictAll();
        emfImp.getSessionFactory().getCache().evictQueryRegions();
        statistics.clear();
        LOGGER.info("loading portfolio {} for the first time", portfolioId);
        Assert.assertFalse(emfImp.getCache().contains(Portfolio.class, portfolioId));
        assertEquals(0, statistics.getSecondLevelCacheHitCount());
        assertEquals(0, statistics.getSecondLevelCacheMissCount());

        em.find(Portfolio.class, portfolioId);
        System.out.println("Enabled Caches: " + StringUtils.arrayToCommaDelimitedString(CacheManager.ALL_CACHE_MANAGERS.get(0).getCacheNames()));
        System.out.println("Statistics: " + emfImp.getSessionFactory().getStatistics());
        System.out.println("Check if Portfolio 1 is in Cache: " + em.getEntityManagerFactory().getCache().contains(Portfolio.class, portfolioId));

        Assert.assertTrue(em.getEntityManagerFactory().getCache().contains(Portfolio.class, portfolioId));
        int size = CacheManager.ALL_CACHE_MANAGERS.get(0).getCache("at.budi.projectdb.persistence.entity.Portfolio").getSize();
        Assert.assertTrue(size > 0);
    }
}